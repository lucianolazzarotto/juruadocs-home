const $ = window.$;

//youtube script
var tag = document.createElement('script');
tag.src = "//www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
var youtubeVideoId = 'e3MO0jDqYwY';

onYouTubeIframeAPIReady = function () {
    player = new YT.Player('intro-video-iframe', {
        videoId: youtubeVideoId,  // youtube video id
        playerVars: {
            'autoplay': 0,
            'controls': 0,
            'autohide': 1,
            'enablejsapi': 1,
            'disablekb': 1,
            'modestbranding': 1,
            'iv_load_policy': 3,
            'rel': 0,
            'fs': 0,
            'showinfo': 0
        },
        events: {
            'onStateChange': onPlayerStateChange
        }
    });
}

var t = document.getElementById ("thumbnail");
t.src = "http://img.youtube.com/vi/"+ youtubeVideoId +"/0.jpg";

onPlayerStateChange = function (event) {
    if (event.data == YT.PlayerState.ENDED) {
        $('.play-video').fadeIn('normal');
    }
}

$(document).on('click', '.play-video', function () {
    $(this).toggleClass('d-md-block');
    $(".thumbnail").toggleClass('d-md-block');
    player.playVideo();
});

var s = skrollr.init({forceHeight: false});

var _skrollr = skrollr.get(); // get() returns the skrollr instance or undefined
var windowWidth = $(window).width();

if ( windowWidth <= 768 && _skrollr !== undefined ) {
  _skrollr.destroy();
}

$('.slider').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$('.slider-promo').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1
});
