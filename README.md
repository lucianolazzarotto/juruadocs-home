Bem vindo ao projeto front-end do sistema Juruádocs.

Para compilar o SCSS foi utilizado o [Gulp](https://gulpjs.com/). Abaixo estão listadas as etapas para instalá-lo.

1. Baixe e instale a versão mais atual do [Node.js](https://nodejs.org/en/)

2. Rode o comando `npm` para instalar as dependências do Gulp requeridas nesse projeto:
`npm install`

3. Dentro do diretório do projeto no seu app de terminal, execute o comando a seguir para compilar as mudanças no arquivos SCSS:
`gulp`

O css compilado irá ser exportado para o seguinte caminho:
`/css/jurua-docs-admin.css`

That's all!